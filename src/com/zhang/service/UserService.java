/**
 * 
 */
package com.zhang.service;

import java.util.ArrayList;
import java.util.List;

import com.zhang.entity.User;

/**
 * @author ZhangHao 
 * @date  2013-7-12
 */
public class UserService {
	private static List<User> userList = new ArrayList<User>();
	
	public User save(User u){
		userList.add(u);
		return u;
	}
	
	public List<User> findAll(){
		return userList;
	}
	
	public void delete(User u){
		userList.remove(u);
	}
}
